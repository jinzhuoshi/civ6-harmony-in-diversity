insert or ignore into IconTextureAtlases
    (Name,                                              IconSize,   IconsPerRow,    IconsPerColumn,     Filename)
values
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_ZHUANG_ZHOU',       160,        1,              1,                  'GreatPeople_ZhuangZhou_160.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_SI_MA_QIAN',        160,        1,              1,                  'GreatPeople_SiNaQian_160.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_ABDUS_SALAM',       160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_ABDUS_SALAM.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_CARAVAGGIO',   160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_CWON_CARAVAGGIO.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_DANTE',        160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_CWON_DANTE.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_JOHN_DONNE',   160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_CWON_JOHN_DONNE.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_MOLIERE',      160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_CWON_MOLIERE.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_RAPHAEL',      160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_CWON_RAPHAEL.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_DANDARA',           160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_DANDARA.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_EMILIE_DU_CHATELET1',160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_EMILIE_DU_CHATELET.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_GUO_SHOU_JING',     160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_GUO_SHOU_JING.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_HD_ALI_MUGHAYAT_SYAH',160,        1,              1,                'GREAT_PERSON_INDIVIDUAL_HD_ALI_MUGHAYAT_SYAH.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_HD_CROESUS',           160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_HD_CROESUS.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_HD_SANG_HONGYANG',   160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_HD_SANG_HONGYANG.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JAMES_YOUNG',        160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_JAMES_YOUNG.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JNR_ARCHIMEDES',       160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_JNR_ARCHIMEDES.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JNR_LI_BING',       160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_JNR_LI_BING.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JNR_MA_JUN',       160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_JNR_MA_JUN.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_KINICH_KAN_BAHLAM',   160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_KINICH_KAN_BAHLAM.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_LEU_JOHN_KEYNES',   160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_LEU_JOHN_KEYNES.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_MARGARET_MEAD',     160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_MARGARET_MEAD.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_MARY_LEAKEY',       160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_MARY_LEAKEY.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_SONGTSAN_GAMPO',    160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_SONGTSAN_GAMPO.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_STEPHANIE_KWOLEK',   160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_STEPHANIE_KWOLEK.dds'),
    ('ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_XU_FU',             160,        1,              1,                  'GREAT_PERSON_INDIVIDUAL_XU_FU.dds');
    
insert or replace into IconDefinitions
    (Name,                                              Atlas,                                              'Index')
values
    ('ICON_GREAT_PERSON_INDIVIDUAL_ZHUANG_ZHOU',        'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_ZHUANG_ZHOU',   0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_SI_MA_QIAN',         'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_SI_MA_QIAN',    0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_ABDUS_SALAM',        'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_ABDUS_SALAM',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_CWON_CARAVAGGIO',    'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_CARAVAGGIO',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_CWON_DANTE',         'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_DANTE',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_CWON_JOHN_DONNE',    'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_JOHN_DONNE',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_CWON_MOLIERE',       'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_MOLIERE',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_CWON_RAPHAEL',       'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_CWON_RAPHAEL',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_DANDARA',            'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_DANDARA',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_EMILIE_DU_CHATELET', 'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_EMILIE_DU_CHATELET1',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_GUO_SHOU_JING',      'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_GUO_SHOU_JING',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_HD_ALI_MUGHAYAT_SYAH','ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_HD_ALI_MUGHAYAT_SYAH',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_HD_CROESUS',         'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_HD_CROESUS',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_HD_SANG_HONGYANG',   'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_HD_SANG_HONGYANG',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_JAMES_YOUNG',        'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JAMES_YOUNG',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_JNR_ARCHIMEDES',     'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JNR_ARCHIMEDES',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_JNR_LI_BING',        'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JNR_LI_BING',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_JNR_MA_JUN',         'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_JNR_MA_JUN',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_KINICH_KAN_BAHLAM',  'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_KINICH_KAN_BAHLAM',0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_LEU_JOHN_KEYNES',    'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_LEU_JOHN_KEYNES',    0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_MARGARET_MEAD',      'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_MARGARET_MEAD',    0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_MARY_LEAKEY',        'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_MARY_LEAKEY',    0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_SONGTSAN_GAMPO',     'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_SONGTSAN_GAMPO',    0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_STEPHANIE_KWOLEK',   'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_STEPHANIE_KWOLEK',    0),
    ('ICON_GREAT_PERSON_INDIVIDUAL_XU_FU',              'ICON_ATLAS_GREAT_PERSON_INDIVIDUAL_XU_FU',    0);



