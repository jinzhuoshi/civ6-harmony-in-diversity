--梁左一
insert or replace into GovernorPromotionModifiers
	(GovernorPromotionType,						ModifierId)
select
	'GOVERNOR_PROMOTION_ZONING_COMMISSIONER',	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1);

insert or replace into Modifiers
	(ModifierId,																ModifierType)
select
	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType,					'MODIFIER_SINGLE_CITY_ATTACH_MODIFIER'
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1) union all
select
	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType || '_MODIFIER',	'MODIFIER_BUILDING_YIELD_CHANGE'
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1);

insert or replace into ModifierArguments
	(ModifierId,																Name,				Value)
select
	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType,					'ModifierId',		'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType || '_MODIFIER'
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1) union all
select
	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType || '_MODIFIER',	'Amount',			YieldChange
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1) union all
select
	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType || '_MODIFIER',	'BuildingType',		BuildingType
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1) union all
select
	'ZONING_COMMISSIONER_' || BuildingType || '_' || YieldType || '_MODIFIER',	'YieldType',		YieldType
from Building_YieldChanges where BuildingType in (select BuildingType from Buildings where IsWonder = 1);
