-- Language: en_US
insert or replace into EnglishText
    (Tag,                                                                       Text)
values
    ("LOC_UNIT_SAPPER_NAME",                                                    "Sapper"),
    ("LOC_UNIT_SAPPER_DESCRIPTION",                                             "Sapper"),
    ("LOC_UNIT_ENGINEER_CORP_NAME",                                             "Engineer Corp"),
    ("LOC_UNIT_ENGINEER_CORP_DESCRIPTION",                                      "Engineer Corp");

insert or replace into LocalizedText
    (Language,      Tag,                                                                        Text)
values
    ("zh_Hans_CN",  "LOC_UNIT_SAPPER_NAME",                                                     "工兵"),
    ("zh_Hans_CN",  "LOC_UNIT_ENGINEER_CORP_NAME",                                              "工程部队"),
    ("zh_Hans_CN",  "LOC_UNIT_SAPPER_DESCRIPTION",                                              "古典时期支援单位"),
    ("zh_Hans_CN",  "LOC_UNIT_ENGINEER_CORP_DESCRIPTION",                                       "工业时代支援单位"),
    ("zh_Hans_CN",  "LOC_ABILITY_ENGINEER_HEAL_HD_NAME",                                        "工程支援"),
    ("zh_Hans_CN",  "LOC_ABILITY_ENGINEER_HEAL_HD_DESCRIPTION",                                 "为一环内单位+10生命回复");