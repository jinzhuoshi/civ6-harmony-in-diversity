--------------------------------
--     Military Gameplay      --
--------------------------------

Utils = ExposedMembers.DLHD.Utils
local mGoodyTimes = 0;

-- Start debug
function EarnMoneyOnConquerCity( capturerID, ownerID, cityID, cityX, cityY )
    local cPlayer = Players[ownerID]
    local pPlayer = Players[capturerID]
    local conquerCity = CityManager.GetCityAt(cityX, cityY)
    local citizen = conquerCity:GetPopulation()
    local buildingID = GameInfo.Buildings['BUILDING_GOV_CONQUEST'].Index
    if cPlayer ~= nil and pPlayer ~= nil and conquerCity ~= nil and citizen ~= nil then 
        local amount = citizen * GlobalParameters.OCCUPATION_GOLD_PER_POP
        if (Utils.HasBuildingWithinCountry(capturerID, buildingID)) then
            amount = amount * 2
        end
        Utils.ChangeGoldBalance(capturerID, amount)
        local message = '[COLOR:ResGoldLabelCS]+' .. tostring(amount) .. '[ENDCOLOR][ICON_Gold]'
        Game.AddWorldViewText(0, message, conquerCity:GetX(), conquerCity:GetY())
    end
end

GameEvents.CityConquered.Add(EarnMoneyOnConquerCity);

function DoubleGoodyHut(iPlayerID,iUnitID,iUnknown1,iUnknown2)
    if(iPlayerID ~= 0)then
        return;
    end
    
    mGoodyTimes = mGoodyTimes+1;
    if(mGoodyTimes%2 == 0)then
        return;
    end

    local pPlayer = Players[iPlayerID];
    local pUnit = UnitManager.GetUnit(iPlayerID, iUnitID);
    local pPlot = Map.GetPlot(pUnit:GetX(), pUnit:GetY());
    ImprovementBuilder.SetImprovementType(pPlot, GameInfo.Improvements["IMPROVEMENT_GOODY_HUT"].Index,63);
    UnitManager.PlaceUnit(pUnit, pUnit:GetX(), pUnit:GetY());

end

--Events.GoodyHutReward.Add(DoubleGoodyHut);

function EngineerAutoRoad(iPlayerID,iUnitID,iX,iY,locallyVisible,stateChange)
    local pPlayer = Players[iPlayerID];
    local pUnit = UnitManager.GetUnit(iPlayerID, iUnitID);
    local plot = Map.GetPlot(iX, iY);

    if(plot:IsWater())then
        return
    end

    if(GameInfo.Units[pUnit:GetType()].UnitType ~= "UNIT_SAPPER") and
    (GameInfo.Units[pUnit:GetType()].UnitType ~= "UNIT_MILITARY_ENGINEER") and
    (GameInfo.Units[pUnit:GetType()].UnitType ~= "UNIT_ENGINEER_CORP")then
        return
    end

    local currentRouteType = plot:GetRouteType(plot);
    local playerRouteType = GetRouteTypeForPlayer(pPlayer);
    if currentRouteType == RouteTypes.NONE or CompareRoutes(playerRouteType,currentRouteType) then
        RouteBuilder.SetRouteType(plot, playerRouteType);
    end
end

function CompareRoutes(a,b)
	return GameInfo.Routes[a].MovementCost < GameInfo.Routes[b].MovementCost;
end

function GetRouteTypeForPlayer(player)
	local route = nil;
	local era = GameInfo.Eras[player:GetEra()];
	for routeType in GameInfo.Routes() do 
		if route == nil then
			route = routeType;
		else
			local prereq_era = GameInfo.Eras[routeType.PrereqEra];
			if prereq_era and era.ChronologyIndex >= prereq_era.ChronologyIndex  then
				route =  routeType;
			end
		end
	end
	return route.Index;
end

Events.UnitMoved.Add(EngineerAutoRoad);