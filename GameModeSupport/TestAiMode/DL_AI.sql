--------------------------------------------------------------------------------
--开局如果有湖或者海，水里出现一条海狗
insert or replace into MajorStartingUnits
    (Unit,                  Era,                NotStartTile,   AiOnly, MinDifficulty)
values
    ('UNIT_ANCIENT_SEADOG', 'ERA_ANCIENT',      1,              1,      'DIFFICULTY_EMPEROR');
--坐地送1海狗

--复活间谍，作为一个低优先级的单位
insert or replace into PseudoYields
    (PseudoYieldType,                       DefaultValue)
values
    ('PSEUDOYIELD_UNIT_SPY',                40);

--AI不爱莫克夏,临时方案

insert or replace into GovernorPromotionModifiers 
    (GovernorPromotionType,                  ModifierId) 
values
    ('GOVERNOR_PROMOTION_CARDINAL_BISHOP',   'BISHOP_AI_LESS_FAITH_POP');
insert or replace into Modifiers    
    (ModifierId,                             ModifierType,                                    Permanent,  SubjectRequirementSetId)
values
    ('BISHOP_AI_LESS_FAITH_POP',              'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_PER_POPULATION', 0,  'PLAYER_IS_AT_LEAST_DEITY_DIFFICULTY_AI');
insert or replace into ModifierArguments
    (ModifierId,                              Name,                   Value)
values
    ('BISHOP_AI_LESS_FAITH_POP',               'YieldType',            'YIELD_FAITH'),
    ('BISHOP_AI_LESS_FAITH_POP',               'Amount',               -3);