--世宗 删除原能力 新增能力书院加2琴3%鼓舞
delete from TraitModifiers where TraitType = 'TRAIT_LEADER_SEJONG';

insert or replace into TraitModifiers
	(TraitType,					ModifierId)
values
	('TRAIT_LEADER_SEJONG',		'HD_SEJONG_DISTRICT_SEOWON_CULTURE'),
	('TRAIT_LEADER_SEJONG',		'HD_SEJONG_DISTRICT_SEOWON_INSPIRATION');

insert or replace into Modifiers
	(ModifierId,										ModifierType,											SubjectRequirementSetId)
values
	('HD_SEJONG_DISTRICT_SEOWON_CULTURE',				'MODIFIER_PLAYER_DISTRICTS_ADJUST_YIELD_CHANGE',		'DISTRICT_IS_DISTRICT_SEOWON_REQUIREMENTS'),
	('HD_SEJONG_DISTRICT_SEOWON_INSPIRATION',			'MODIFIER_PLAYER_DISTRICTS_ATTACH_MODIFIER',			'DISTRICT_IS_DISTRICT_SEOWON_REQUIREMENTS'),
	('HD_SEJONG_DISTRICT_SEOWON_INSPIRATION_MODIFIER',	'MODIFIER_PLAYER_ADJUST_CIVIC_BOOST',					NULL);

insert or replace into ModifierArguments
	(ModifierId,										Name,				Value)
values
	('HD_SEJONG_DISTRICT_SEOWON_CULTURE',				'YieldType',		'YIELD_CULTURE'),
	('HD_SEJONG_DISTRICT_SEOWON_CULTURE',				'Amount',			2),
	('HD_SEJONG_DISTRICT_SEOWON_INSPIRATION',			'ModifierId',		'HD_SEJONG_DISTRICT_SEOWON_INSPIRATION_MODIFIER'),
	('HD_SEJONG_DISTRICT_SEOWON_INSPIRATION_MODIFIER',	'Amount',			3);

--狄奥多拉la拜占庭的宠儿：圣地和跑马场建成时，开发相邻的加成和奢侈资源。创力宗教时可以额外选取一个信条。
delete from TraitModifiers where TraitType = 'TRAIT_LEADER_THEODORA';
insert or replace into TraitModifiers
	(TraitType,							ModifierId)
select
	'TRAIT_LEADER_THEODORA',			'THEODORA_ADD_BELIEF'
where exists (select Type from Types where Type = 'TRAIT_LEADER_THEODORA') union all
select
	'TRAIT_LEADER_THEODORA',			'THEODORA_DISTRICT_HOLY_SITE_CULTURE_BOMB'
where exists (select Type from Types where Type = 'TRAIT_LEADER_THEODORA') union all
select
	'TRAIT_LEADER_THEODORA',			'THEODORA_DISTRICT_HIPPODROME_CULTURE_BOMB'
where exists (select Type from Types where Type = 'TRAIT_LEADER_THEODORA');
insert or replace into Modifiers
	(ModifierId,										ModifierType,									SubjectRequirementSetId)
values
	('THEODORA_ADD_BELIEF',								'MODIFIER_PLAYER_ADD_BELIEF',					'PLAYER_HAS_FOUNDED_A_RELIGION'),
	('THEODORA_DISTRICT_HOLY_SITE_CULTURE_BOMB',		'MODIFIER_PLAYER_ADD_CULTURE_BOMB_TRIGGER',		NULL),
	('THEODORA_DISTRICT_HIPPODROME_CULTURE_BOMB',		'MODIFIER_PLAYER_ADD_CULTURE_BOMB_TRIGGER',		NULL);
insert or replace into ModifierArguments
	(ModifierId,										Name,						Value)
values
	('THEODORA_DISTRICT_HOLY_SITE_CULTURE_BOMB',		'CaptureOwnedTerritory',	0),
	('THEODORA_DISTRICT_HOLY_SITE_CULTURE_BOMB',		'DistrictType',				'DISTRICT_HOLY_SITE'),
	('THEODORA_DISTRICT_HIPPODROME_CULTURE_BOMB',		'CaptureOwnedTerritory',	0),
	('THEODORA_DISTRICT_HIPPODROME_CULTURE_BOMB',		'DistrictType',				'DISTRICT_HIPPODROME');
insert or replace into RequirementSets
	(RequirementSetId,						RequirementSetType)
values
	('PLAYER_HAS_FOUNDED_A_RELIGION',		'REQUIREMENTSET_TEST_ALL');
insert or replace into RequirementSetRequirements
	(RequirementSetId,						RequirementId)
values
	('PLAYER_HAS_FOUNDED_A_RELIGION',		'REQUIRES_PLAYER_HAS_FOUNDED_A_RELIGION');
